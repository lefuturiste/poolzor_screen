
let serial = (new URLSearchParams(document.location.search)).get('serial')
//const socket = io("http://localhost:3042/?serial=" + serial);
const socket = io("http://poolzor-screen-websocket.werobot.fr/?serial=" + serial);

window.socketGlob = socket

socket.on("connection", (socket) => {
    console.log('connected')
    console.log(socket.id); // x8WIv7-mJelg7on_ALbx
});

const iframe = document.getElementById("iframe")

let lastSrc = ''

const debugContainer = document.getElementById('debug-container')
const debugSerial = document.getElementById('debug-serial')
const debugPurpose = document.getElementById('debug-purpose')
const identifyContainer = document.getElementById('identify-container')

debugSerial.innerText = serial

let params = {}

const wholeScreenContainer = document.getElementById('whole-screen-container')

socket.on("update", (data) => {
    console.log('got topic update', data.topic, data.payload)
    if (data.destination != serial) {
        return
    }
    if (data.topic == 'params') {
        params = data.payload
        debugPurpose.innerText = params.label
        wholeScreenContainer.className = ''
        if (params.rotation == 'left') wholeScreenContainer.classList.add('whole-screen-rotated-left')
        if (params.rotation == 'right') wholeScreenContainer.classList.add('whole-screen-rotated-right')
    }
    if (data.topic == 'iframeSrc') {
        iframe.src = data.payload
        lastSrc = data.payload
    }
    if (data.topic == 'identify') {
        if (data.payload) {
            identifyContainer.style.display = 'block'
        } else {
            identifyContainer.style.display = 'none'
        }
    }
    if (data.topic == 'debug') {
        if (data.payload) {
            debugContainer.style.display = 'block'
        } else {
            debugContainer.style.display = 'none'
        }
    }
    if (data.topic == 'refreshIframe') {
        iframe.src = 'https://ping.lefuturiste.fr'
        setTimeout(() => {
            iframe.src = lastSrc
        }, 300)
    }
    if (data.topic == 'refreshGuard') {
        window.location.reload()
    }
})

// implement zombie page

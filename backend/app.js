const app = require('express')()
const cors = require('cors')
const expand = require('expand-template')()

app.use(cors())

const httpServer = require('http').createServer(app)
const io = require('socket.io')(httpServer, {
    cors: {
        origin: '*',
        methods: ['GET', 'POST']
    }
})

app.get('/', (req, res) => {
    res.json(true)
})

let remoteScreens = []

const baseInstance = 'http://cdfrjr.live.poolzor'
let screenConfiguration = [ 
    { 
        serial: 'test',
        params: { role: 'public', label: 'lel', rotation: 'left' },
        src: 'https://src.lefuturiste.fr/texts/french.txt'
    },
    {
        serial: '000000009a68eab9',
        params: { rotation: 'right', role: 'table',  tableId: 3, label: 'table_3' },
        src: baseInstance + '/print.php?page=retour_affichage_table&table={tableId}'
    },
    {
        serial: '000000009c914646',
        params: { rotation: 'right', role: 'table',  tableId: 2, label: 'table_2' },
        src: baseInstance + '/print.php?page=retour_affichage_table&table={tableId}'
    },
    {
        serial: '00000000e240f6c2',
        params: { rotation: 'right', role: 'table',  tableId: 1, label: 'table_1' },
        src: baseInstance + '/print.php?page=retour_affichage_table&table={tableId}'
    },
    {
        serial: '00000000279ae2a4',
        params: { rotation: 'right', role: 'table',  tableId: 5, label: 'table_5' },
        src: baseInstance + '/print.php?page=retour_affichage_table&table={tableId}'
    },
    {
        serial: '000000002bde742d',
        params: { rotation: 'right', role: 'table',  tableId: 4, label: 'table_4' },
        src: baseInstance + '/print.php?page=retour_affichage_table&table={tableId}'
    },
    {
        serial: '00000000b8b18038',
        params: { rotation: 0, role: 'public', label: 'osef1' },
        src: 'http://screen.poolzor/public.html'
    },
    {
        serial: '00000000ea6ef0a8',
        params: { rotation: 0, role: 'public', label: 'osef2' },
        src: 'http://screen.poolzor/public.html'
    },
    {
        serial: '00000000985c9d7a',
        params: { rotation: 0, role: 'public', label: 'osef3' },
        src: 'http://screen.poolzor/public.html'
    }
]

// evaluate screenConfiguration
screenConfiguration = screenConfiguration.map(s => {
    if (s.src) {
        s.src = expand(s.src, s.params)
    }
    return s
})

const sendUpdate = (destination, topic, payload) => {
    const screen = remoteScreens.find(s => s.serial === destination)
    console.log('  sent update', {
        destination,
        topic,
        payload
    })
    screen.socket.emit('update', {
        destination,
        topic,
        payload
    })
}

let doShowDebug = false
app.get('/debug/toggle', (req, res) => {
    doShowDebug = !doShowDebug
    console.log(`> Toggle debug now at: ${doShowDebug}`)
    remoteScreens.forEach(s => {
        sendUpdate(s.serial, 'debug', doShowDebug)
    })

    res.json(true)
})


app.get('/debug/identify/:serial', (req, res) => {
    console.log(req.params.serial)

    const screen = remoteScreens.find(s => s.serial === req.params.serial)
    if (!screen) {
        res.json(false)
        return
    }
    screen.identify = !screen.identify
    remoteScreens = remoteScreens.map(s => {
        if (s.serial == screen.serial) s = screen
        return s
    })
    sendUpdate(screen.serial, 'identify', screen.identify)

    res.json(true)
})


app.get('/refresh-guard', (req, res) => {
    remoteScreens.forEach(s => {
        sendUpdate(s.serial, 'refreshGuard', true)
    })
    res.json(true)
})
app.get('/refresh-iframe', (req, res) => {
    remoteScreens.forEach(s => {
        sendUpdate(s.serial, 'refreshIframe', true)
    })
    res.json(true)
})

app.get('/screens', (req, res) => {
    res.json(remoteScreens.map(sObj => {
        let s = { ...sObj }
        s.configuration = screenConfiguration.find(sb => sb.serial == s.serial)
        if (!s.configuration) {
            s._msg = "Invalid serial"
        }
        delete s.socket
        return s
    }))
})

io.on('connection', socket => {
    let newSerial = socket.handshake.query.serial
    if (!newSerial) {
        console.log('> Kicked a socket connexion (no serial query param)')
        socket.disconnect()
        return
    }
    console.log(`> New connection, serial: ${newSerial}`)
    if (!screenConfiguration.find(s => s.serial == newSerial)) {
        console.log('> Kicked a socket connexion (invalid serial id)')
        socket.disconnect()
        return
    }
    if (remoteScreens.find(s => s.serial == newSerial)) {
        remoteScreens = remoteScreens.filter(s => s.serial != newSerial)
        console.log('> Overwrote connection')
    }
    remoteScreens.push({
        serial: newSerial,
        idConn: socket.id,
        socket
    })
    let conf = screenConfiguration.find(sb => sb.serial == newSerial)

    // sync state
    sendUpdate(newSerial, 'iframeSrc', conf.src)
    sendUpdate(newSerial, 'debug', doShowDebug)
    sendUpdate(newSerial, 'params', conf.params)

    socket.on('disconnect', () => {
        remoteScreens = remoteScreens.filter(s => socket.id != s.idConn)
    })
})

const host = process.env.HOST == null ? '0.0.0.0' : process.env.HOST
const port = process.env.PORT == null ? 4227 : process.env.PORT

console.log(`> Starting a carnage server...`)
httpServer.listen(port, host, () => {
    console.log(`> Started a carnage server on http://` + host + `:` + port)
})
